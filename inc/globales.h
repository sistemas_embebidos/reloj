#ifndef GLOBALES_H
#define GLOBALES_H

#include "stdint.h"
#include "stdbool.h"

extern uint16_t MAX_INTERRUPCIONES;
uint16_t TIMEOUT_CONFIG;
extern uint8_t hora[4];
extern uint8_t hora_temp[4];
extern uint8_t segundos;
extern uint16_t contador_interrupciones;
extern uint8_t hora_alarma[4];
uint16_t contador_enter;
extern uint8_t timeout_modo_config;

extern bool sirena_encencida;
extern bool alarma_activada;
extern bool apagar_sirena;
extern bool punto_prendido;
extern bool sirena_sonando;
extern enum {MODO_NORMAL, MODO_ALARMA, MODO_HORA} Menu;

#endif // GLOBALES_H

