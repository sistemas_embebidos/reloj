#ifndef HORA_H
#define HORA_H

#include "stdint.h"

void incrementar_numero(uint8_t[4]);
void incrementar_hora(uint8_t[4]);
void decrementar_hora(uint8_t[4]);
void incrementar_minuto(uint8_t[4]);
void decrementar_minuto(uint8_t[4]);
void decrementar_numero(uint8_t[4]);
void incrementar_contador_interrupciones();

void chequear_alarma();
void apagarAlarma();
void toggleAlarma();
void setConfigTime();
void chequeoEnter();
void setConfigAlarma();
void irModoNormal();
#endif // HORA_H

