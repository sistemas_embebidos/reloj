#include "unity.h"
#include "hora.h"
#include "stdint.h"
#include "globales.h"

void test_incrementar_numero()
{
    MAX_INTERRUPCIONES = 10;
    uint8_t hora[4] = {0,0,0,0};
    uint8_t hora_esperada[4] = {0,0,0,1};
    incrementar_numero(hora);
    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora,hora_esperada,4,"paso la hora");

    uint8_t hora2[4] = {0,0,0,9};
    uint8_t hora_esperada2[4] = {0,0,1,0};
    incrementar_numero(hora2);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora2,hora_esperada2,4,"paso la hora");

    uint8_t hora3[4] = {0,0,5,9};
    uint8_t hora_esperada3[4] = {0,1,0,0};
    incrementar_numero(hora3);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora3,hora_esperada3,4,"paso la hora");

    uint8_t hora4[4] = {0,9,5,9};
    uint8_t hora_esperada4[4] = {1,0,0,0};
    incrementar_numero(hora4);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora4,hora_esperada4,4,"paso la hora");

    uint8_t hora5[4] = {2,3,5,9};
    uint8_t hora_esperada5[4] = {0,0,0,0};
    incrementar_numero(hora5);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora5,hora_esperada5,4,"paso la hora");
}

void test_decrementar_numero()
{
        MAX_INTERRUPCIONES = 10;
    uint8_t hora[4] = {0,0,0,1};
    uint8_t hora_esperada[4] = {0,0,0,0};
    decrementar_numero(hora);
    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora,hora_esperada,4,"paso la hora");

    uint8_t hora2[4] = {0,0,1,0};
    uint8_t hora_esperada2[4] = {0,0,0,9};
    decrementar_numero(hora2);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora2,hora_esperada2,4,"paso la hora");

    uint8_t hora3[4] = {0,1,0,0};
    uint8_t hora_esperada3[4] = {0,0,5,9};
    decrementar_numero(hora3);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora3,hora_esperada3,4,"paso la hora");

    uint8_t hora4[4] = {1,0,0,0};
    uint8_t hora_esperada4[4] = {0,9,5,9};
    decrementar_numero(hora4);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora4,hora_esperada4,4,"paso la hora");

    uint8_t hora5[4] = {0,0,0,0};
    uint8_t hora_esperada5[4] = {2,3,5,9};
    decrementar_numero(hora5);

    TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(hora5,hora_esperada5,4,"paso la hora");
}
