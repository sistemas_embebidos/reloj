#include "stdint.h"
#include "stdbool.h"

uint16_t MAX_INTERRUPCIONES = 2200;
uint16_t TIMEOUT_CONFIG = 5;
uint8_t hora[4] = {0, 0, 0, 0};
uint8_t hora_temp[4] = {0, 0, 0, 0};
uint8_t segundos;
uint16_t contador_interrupciones;
uint8_t hora_alarma[4] = {0, 0, 0, 1};
uint16_t contador_enter = 0;
uint8_t timeout_modo_config = 0;

enum {MODO_NORMAL, MODO_ALARMA, MODO_HORA} Menu;

bool sirena_encencida = false;
bool sirena_sonando = false;
bool alarma_activada = false;
bool apagar_sirena = true;
bool punto_prendido = false;

