#include "hora.h"
#include "stdint.h"
#include "stdbool.h"
#include "globales.h"

void incrementar_numero(uint8_t hora[4])
{
    if (hora[3] == 9) {
        hora[3] = 0;
        if (hora[2] == 5) {
            hora[2] = 0;
            incrementar_hora(hora);
        } else {
            hora[2]++;
        }
    } else {
        hora[3]++;
    }

    //cuando paso el dia entero volvemos a resetear la variable apgar sirena
    if (hora[0] == 0 && hora[1] == 0 && hora[1] == 0 && segundos == 0) {
        apagar_sirena = true;
    }

}

void incrementar_contador_interrupciones()
{
    contador_interrupciones++;
    if (contador_interrupciones == MAX_INTERRUPCIONES) {
        if (segundos == 59) {
            segundos = 0;
            incrementar_numero(hora);
        } else {
            segundos++;
        }
        punto_prendido = !punto_prendido;
        if (sirena_encencida) {
            sirena_sonando = !sirena_sonando;
        }
        contador_interrupciones = 0;
        if (Menu != MODO_NORMAL) {
            timeout_modo_config++;
        }
    }
}

void incrementar_minuto(uint8_t hora[4])
{
    if (hora[3] == 9) {
        hora[3] = 0;
        if (hora[2] == 5) {
            hora[2] = 0;
        } else {
            hora[2]++;
        }
    } else {
        hora[3]++;
    }
}

void decrementar_minuto(uint8_t hora[4])
{
    if (hora[3] == 0) {
        hora[3] = 9;
        if (hora[2] == 0) {
            hora[2] = 5;
        } else {
            hora[2]--;
        }
    } else {
        hora[3]--;
    }
}

void incrementar_hora(uint8_t hora[4])
{
    if (hora[0] == 2 && hora[1] == 3) {
        hora[0] = 0;
        hora[1] = 0;
    } else {
        if (hora[1] == 9) {
            hora[1] = 0;
            hora[0]++;
        } else {
            hora[1]++;
        }
    }
}

void decrementar_hora(uint8_t hora[4])
{
    if (hora[1] == 0 && hora[0] == 0) {
        hora[0] = 2;
        hora[1] = 3;
    } else {
        if (hora[1] == 0) {
            hora[1] = 9;
            hora[0]--;
        } else {
            hora[1]--;
        }
    }
}

void decrementar_numero(uint8_t hora[4])
{

    if (hora[3] == 0) {
        hora[3] = 9;
        if (hora[2] == 0) {
            hora[2] = 5;
            if (hora[1] == 0 && hora[0] == 0) {
                hora[0] = 2;
                hora[1] = 3;
            } else {
                if (hora[1] == 0) {
                    hora[1] = 9;
                    hora[0]--;
                } else {
                    hora[1]--;
                }
            }
        } else {
            hora[2]--;
        }
    } else {
        hora[3]--;
    }
}

void chequear_alarma()
{
    if (alarma_activada) {
        if(hora[0] == hora_alarma[0] && hora[1] == hora_alarma[1] && hora[2] == hora_alarma[2] && hora[3] == hora_alarma[3]) {
            if (apagar_sirena && !sirena_encencida) {
                sirena_encencida = true;
                //apagar_sirena = false;
                sirena_sonando = true;
                //set_sirena(sirena_sonando);
            }
        }
    }
}

void apagarAlarma()
{
    sirena_encencida = false;
    sirena_sonando = false;
    apagar_sirena = false;
}

//activa/desactiva la alarma
void toggleAlarma()
{
    alarma_activada = !alarma_activada;
    if (sirena_encencida) {
        apagarAlarma();
    }
}

void setConfigTime() {
    Menu = MODO_HORA;
    timeout_modo_config = 0;
    for(int loop = 0; loop < 4; loop++) {
      hora_temp[loop] = hora[loop];
    }

}

void setConfigAlarma() {
    Menu = MODO_ALARMA;
    timeout_modo_config = 0;
    for(int loop = 0; loop < 4; loop++) {
      hora_temp[loop] = hora_alarma[loop];
    }

}

void chequeoEnter() {
    if (Menu != MODO_NORMAL) {
        if (contador_enter == 0) {
            contador_enter++;
        } else {
            if (Menu == MODO_ALARMA) {
                for(int loop = 0; loop < 4; loop++) {
                    hora_alarma[loop] = hora_temp[loop];
                }
                apagar_sirena = true;
            } else {
                if (Menu == MODO_HORA) {
                    for(int loop = 0; loop < 4; loop++) {
                        hora[loop] = hora_temp[loop];
                    }    
                }
            }
            irModoNormal();
        }   
    }
    apagarAlarma();
}

void irModoNormal() {
    contador_enter = 0;
    timeout_modo_config = 0;
    Menu = MODO_NORMAL;
}

