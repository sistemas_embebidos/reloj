/* Copyright 2014, 2015 Mariano Cerdeiro
 * Copyright 2014, Pablo Ridolfi
 * Copyright 2014, Juan Cecconi
 * Copyright 2014, Gustavo Muro
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "baremetal.h"       /* <= own header */
#include "stdbool.h"
#include "globales.h"
#include "hora.h"

#ifndef CPU
#error CPU shall be defined
#endif
#if (lpc4337 == CPU)
#include "chip.h"
#elif (mk60fx512vlq15 == CPU)
#else
#endif

/*==================[macros and definitions]=================================*/

#define SCU_MODE_SAL (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS)
#define SCU_MODE_ENT (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN)

#define SW_GPIO_PORT    5
#define SW1_GPIO_PIN    15
#define SW2_GPIO_PIN    14
#define SW3_GPIO_PIN    13
#define SW4_GPIO_PIN    12
#define SW5_GPIO_PIN    8
#define SW6_GPIO_PIN    9

#define CANTIDAD_DIGITOS 4

#define apagarSegmentos()              Chip_GPIO_ClearValue(LPC_GPIO_PORT, 2, 0x7F)
#define prenderSegmentos(mascara)      Chip_GPIO_SetValue(LPC_GPIO_PORT, 2, (mascara) & 0x7F)
#define apagarDigitos()                Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0x0F)
#define prenderDigito(digito)          Chip_GPIO_SetValue(LPC_GPIO_PORT, 0, (1 << (digito)) & 0x0F)
#define prenderPunto(valor)            Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 16, valor)

#define NO_KEY    0

//! Valor de retorno cuando esta presionada la tecla F1
#define TECLA_F1  1

//! Valor de retorno cuando esta presionada la tecla F2
#define TECLA_F2  2

//! Valor de retorno cuando esta presionada la tecla F3
#define TECLA_F3  3

//! Valor de retorno cuando esta presionada la tecla F4
#define TECLA_F4  4

//! Valor de retorno cuando esta presionada la tecla SI
#define TECLA_UP  5

//! Valor de retorno cuando esta presionada la tecla NO
#define TECLA_DOWN  6

#define COLOR_ROJO  0
#define COLOR_VERDE  1
#define COLOR_AZUL  2

/*==================[internal data declaration]==============================*/


const uint8_t digitos[] = {
   0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x67
};

uint8_t numero[4] = { 0, 0, 0, 0};

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

void configurarPuertos(void);

void configurarInterrupcion(void);

/*==================[external functions definition]==========================*/
/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */
void configurarInterrupcion(void) {
  /* Deshabilita interrupciones */
   asm volatile ("cpsid i");

   /* Activate SysTick */
   SystemCoreClockUpdate();
   SysTick_Config(SystemCoreClock/MAX_INTERRUPCIONES);

   /* Update priority set by SysTick_Config */
   NVIC_SetPriority(SysTick_IRQn, (1  <<__NVIC_PRIO_BITS) - 1);

  /* Habilita interrupciones */
   asm volatile ("cpsie i");
}

 void inicializarSistema() {
    Menu = MODO_NORMAL;
    Chip_GPIO_Init(LPC_GPIO_PORT);
    configurarLeds();
    configurarTeclas();
    configurarDigitos();  
    configurarRGB();
    apagarSegmentos();
    Led_RGB(0, 0, 0);
 }

void configurarPuertos(void) {

   //Chip_GPIO_SetValue(LPC_GPIO_PORT, 2, 0x7F); //prende todos los led
   //Chip_GPIO_SetDir(LPC_GPIO_PORT, 2, 0x7F, 1);

   Chip_GPIO_SetValue(LPC_GPIO_PORT, 0, 0x1);
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0xF);
   Chip_GPIO_SetValue(LPC_GPIO_PORT, 0, 0x2);
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0xF);
   Chip_GPIO_SetValue(LPC_GPIO_PORT, 0, 0x4);
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0xF);
   Chip_GPIO_SetValue(LPC_GPIO_PORT, 0, 0x8);
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, 0xF);

   /* todos los LED del display a 1 */
   /* multiplex a 0 */
   /* Chip_GPIO_SetDir(LPC_GPIO_PORT, port, mask, output); */
   //Chip_GPIO_SetDir(LPC_GPIO_PORT, 5, (1 << 0) | (1 << 1) | (1 << 2), 1);

}

void configurarTeclas(void) {

   /* Puerto Teclas 1 a 4 */
   Chip_SCU_PinMux(4, 8, SCU_MODE_ENT, SCU_MODE_FUNC4);
   Chip_SCU_PinMux(4, 9, SCU_MODE_ENT, SCU_MODE_FUNC4);
   Chip_SCU_PinMux(4, 10, SCU_MODE_ENT, SCU_MODE_FUNC4);
   Chip_SCU_PinMux(6, 7, SCU_MODE_ENT, SCU_MODE_FUNC4);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, SW_GPIO_PORT, (1 << SW1_GPIO_PIN) | (1 << SW2_GPIO_PIN), false);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, SW_GPIO_PORT, (1 << SW2_GPIO_PIN) | (1 << SW3_GPIO_PIN), false);

   /* Puerto Teclas Aceptar y Cancelar */
   Chip_SCU_PinMux(3, 1, SCU_MODE_ENT, SCU_MODE_FUNC4);
   Chip_SCU_PinMux(3, 2, SCU_MODE_ENT, SCU_MODE_FUNC4);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, SW_GPIO_PORT, (1 << SW5_GPIO_PIN) | (1 << SW6_GPIO_PIN), false);
}

void configurarLeds(void) {
   /* Puerto Led RGB */
   Chip_SCU_PinMux(2, 0, SCU_MODE_SAL, SCU_MODE_FUNC4);
   Chip_SCU_PinMux(2, 1, SCU_MODE_SAL, SCU_MODE_FUNC4);
   Chip_SCU_PinMux(2, 2, SCU_MODE_SAL, SCU_MODE_FUNC4);
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 5, (1 << 0) | (1 << 1) | (1 << 2));
   Chip_GPIO_SetDir(LPC_GPIO_PORT, 5, (1 << 0) | (1 << 1) | (1 << 2), 1);

   /* Puerto Leds 1 a 3 */
   Chip_SCU_PinMux(2, 10, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(2, 11, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(2, 12, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0, (1 << 14));
   Chip_GPIO_ClearValue(LPC_GPIO_PORT, 1, (1 << 11) | (1 << 12));
   Chip_GPIO_SetDir(LPC_GPIO_PORT, 0, (1 << 14), true);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, 1, (1 << 11) | (1 << 12), true);
}

void configurarDigitos() {
   apagarDigitos();
   apagarSegmentos();
   prenderPunto(false);

   /* Pines de los digitos */
   Chip_SCU_PinMux(0, 0, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(0, 1, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(1, 15, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(1, 17, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, 0, 0x0F, 1);

   /* Pines de los segmentos */
   Chip_SCU_PinMux(4, 0, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(4, 1, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(4, 2, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(4, 3, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(4, 4, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(4, 5, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_SCU_PinMux(4, 6, SCU_MODE_SAL, SCU_MODE_FUNC0);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, 2, 0x7F, 1);

   /* Pin del punto */
   Chip_SCU_PinMux(6, 8, SCU_MODE_SAL, SCU_MODE_FUNC4);
   Chip_GPIO_SetDir(LPC_GPIO_PORT, 5, 0x10000, 1);
}

void configurarRGB(void) {
   /* Puerto Leds RGB */
   Chip_SCU_PinMux(1, 3, SCU_MODE_SAL, SCU_MODE_FUNC1);
   Chip_SCU_PinMux(1, 4, SCU_MODE_SAL, SCU_MODE_FUNC1);
   Chip_SCU_PinMux(1, 5, SCU_MODE_SAL, SCU_MODE_FUNC1);

   Chip_SCTPWM_Init(LPC_SCT);
   Chip_SCTPWM_SetRate(LPC_SCT, 10000);
   Chip_SCTPWM_SetOutPin(LPC_SCT, 1, 8);
   Chip_SCTPWM_SetOutPin(LPC_SCT, 2, 9);
   Chip_SCTPWM_SetOutPin(LPC_SCT, 3, 10);
   Chip_SCTPWM_Start(LPC_SCT);
}

void Led_RGB(uint8_t r, uint8_t g, uint8_t b) {
   if ((r <= 100) & (g <= 100) & (b <= 100)) {
      Chip_SCTPWM_SetDutyCycle(LPC_SCT, 1, Chip_SCTPWM_PercentageToTicks(LPC_SCT, 100 - r));
      Chip_SCTPWM_SetDutyCycle(LPC_SCT, 2, Chip_SCTPWM_PercentageToTicks(LPC_SCT, 100 - g));
      Chip_SCTPWM_SetDutyCycle(LPC_SCT, 3, Chip_SCTPWM_PercentageToTicks(LPC_SCT, 100 - b));
   }
}

void Escribir_Segmentos(uint8_t segmentos, uint8_t posicion) {
   if (posicion < CANTIDAD_DIGITOS) {
      apagarDigitos();
      apagarSegmentos();
      prenderSegmentos(segmentos);
      prenderDigito(CANTIDAD_DIGITOS - 1 - posicion);
   }
}

void Escribir_Digito(uint8_t valor, uint8_t posicion) {
   if (posicion < CANTIDAD_DIGITOS) {
      apagarDigitos();
      apagarSegmentos();
      prenderSegmentos(digitos[valor & 0x0F]);
      prenderDigito(CANTIDAD_DIGITOS - 1 - posicion);
   }
}


void refrescarDigitos(void) {
   static int activo = 0;

   activo = ((activo + 1) & 0x03);

  if (Menu == MODO_NORMAL) {
    prenderPunto(activo == 1 && punto_prendido);
    Escribir_Digito(hora[activo], activo);
  } else {
    prenderPunto(false);  
    if (contador_interrupciones < MAX_INTERRUPCIONES / 2) {
        if (contador_enter == 0 && (activo == 2 || activo == 3)) {
            Escribir_Digito(hora_temp[activo], activo);
        } else {
          if (contador_enter == 1 && (activo == 0 || activo == 1)) {
            Escribir_Digito(hora_temp[activo], activo);
          }
        }  
    } else {
        Escribir_Digito(hora_temp[activo], activo);
    }
  }
   
 }

 uint8_t Leer_Teclas(void) {
   uint8_t switchPressed = NO_KEY;

   if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SW_GPIO_PORT,SW1_GPIO_PIN))
      switchPressed = TECLA_F1;

   if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SW_GPIO_PORT,SW2_GPIO_PIN))
      switchPressed = TECLA_F2;

   if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SW_GPIO_PORT,SW3_GPIO_PIN))
      switchPressed = TECLA_F3;

   if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SW_GPIO_PORT,SW4_GPIO_PIN))
      switchPressed = TECLA_F4;

   if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SW_GPIO_PORT,SW5_GPIO_PIN))
      switchPressed = TECLA_UP;

   if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SW_GPIO_PORT,SW6_GPIO_PIN))
      switchPressed = TECLA_DOWN;

   return switchPressed;
}

void LedColor(int color) {
  switch (color) {
    case COLOR_AZUL:
      Led_RGB(90, 0, 0);
      break;
    case COLOR_ROJO:
      Led_RGB(0,90,0);
      break;
    case COLOR_VERDE:
      Led_RGB(0,0,90);
      break;
    default:
      Led_RGB(0, 0, 0);  
  }
}

void setSirena(bool estado){
  if (estado) {
    LedColor(COLOR_ROJO);     
  } else {
    LedColor(-1);
  }
}


int main(void)
{

  static uint8_t anterior = 0;
  uint8_t actual;

  inicializarSistema();

  configurarInterrupcion();

   while (1) {
      actual = Leer_Teclas();
      if (actual != anterior) {
         switch(actual) {
            case TECLA_F1:
               LedColor(COLOR_VERDE);
               setConfigTime();
               break;
            case TECLA_F2:
                LedColor(COLOR_AZUL);
                setConfigAlarma();
               break;
            case TECLA_F3:
               timeout_modo_config = 0;
               chequeoEnter();
               if (Menu == MODO_NORMAL) {
                 LedColor(-1);
               }
               break;
            case TECLA_F4:
               toggleAlarma();
               break;
            case TECLA_UP:
              if (Menu != MODO_NORMAL) {
                  timeout_modo_config = 0;
                  if (contador_enter == 0) {
                    incrementar_hora(hora_temp);
                  } else {
                    incrementar_minuto(hora_temp);
                  }  
              }
               break;
            case TECLA_DOWN:
              if (Menu != MODO_NORMAL) {
                  timeout_modo_config = 0;
                  if (contador_enter == 0) {
                    decrementar_hora(hora_temp);
                  } else {
                    decrementar_minuto(hora_temp);
                  }  
              }
            break;
         }
         anterior = actual;
      }
   }

  return 0;
}

/**
 * @brief Funcion periodica que se ejecuta con el SysTick
 */
void funcionPeriodica(void) {
   static int contador = 0;

   static int divisor = 0;

   divisor = (divisor + 1) % 10;
   if (divisor == 0) {
      refrescarDigitos();
   }
  incrementar_contador_interrupciones();
  chequear_alarma();
  if (Menu == MODO_NORMAL) {
    setSirena(sirena_sonando);
  } else {
    if (timeout_modo_config == TIMEOUT_CONFIG) {
      irModoNormal();
    }
  }
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

